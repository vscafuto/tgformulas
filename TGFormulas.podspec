#
# Be sure to run `pod lib lint TGFormulas.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "TGFormulas"
  s.version          = "1.0.0"
  s.summary          = "TGFormulas contiene la maggior parte delle formule di conversione utilizzate nelle app mobile MyWellness."
  s.homepage         = "https://bitbucket.org/vscafuto/tgformulas"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Vincenzo Scafuto" => "vincenzo@scafuto.it" }
  s.source           = { :git => "https://bitbucket.org/vscafuto/tgformulas.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'TGFormulas' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
