# TGFormulas

[![CI Status](http://img.shields.io/travis/Vincenzo Scafuto/TGFormulas.svg?style=flat)](https://travis-ci.org/Vincenzo Scafuto/TGFormulas)
[![Version](https://img.shields.io/cocoapods/v/TGFormulas.svg?style=flat)](http://cocoadocs.org/docsets/TGFormulas)
[![License](https://img.shields.io/cocoapods/l/TGFormulas.svg?style=flat)](http://cocoadocs.org/docsets/TGFormulas)
[![Platform](https://img.shields.io/cocoapods/p/TGFormulas.svg?style=flat)](http://cocoadocs.org/docsets/TGFormulas)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TGFormulas is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "TGFormulas"

## Author

Vincenzo Scafuto, vincenzo.scafuto@studio.unibo.it

## License

TGFormulas is available under the MIT license. See the LICENSE file for more info.

