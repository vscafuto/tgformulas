//
//  TGFormulas.h
//  Technogym App
//
//  Created by Vincenzo Scafuto on 05/03/15.
//  Copyright (c) 2015 Vincenzo Scafuto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGFormulas : NSObject

+(double)calculateMetsFromDistance:(double)distance duration:(double)duration;
+(double)calculateCaloriesFromMoves:(double)moves userWeight:(double)userWeight;
+(double)calculateMovesFromDistance:(double)distance duration:(double)duration;
+(double)calculateMovesFromMets:(double)mets duration:(double)duration;
@end
