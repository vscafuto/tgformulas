//
//  TGFormulas.m
//  Technogym App
//
//  Created by Vincenzo Scafuto on 05/03/15.
//  Copyright (c) 2015 Vincenzo Scafuto. All rights reserved.
//

#import "TGFormulas.h"

@implementation TGFormulas


+(double)calculateMetsFromDistance:(double)distance duration:(double)duration
{
    double speed = (distance/duration) * 3.6f;
    double grade = 0.0f;
    double nVelLow = 5.0f;
    double nVelHigh = 7.0f;
    double lowVO2 = nVelLow * 5 / 3 + nVelLow * grade * 0.3 + 3.5;
    double highVO2 = nVelHigh * 10 / 3 + nVelHigh * grade * 0.15 + 3.5;
    double MM = (highVO2 - lowVO2) / (nVelHigh - nVelLow);
    double NN = lowVO2 - MM * nVelLow;
    double VO2 = 0.0f;
    
    if (speed <= nVelLow)
        VO2 = speed * 5 / 3 + speed * grade * 0.3 + 3.5;
    else if (speed >= nVelHigh)
        VO2 = speed * 10 / 3 + speed * grade * 0.15 + 3.5;
    else
        VO2 = speed * MM + NN;
    
    return VO2 / 3.5f;
}

+(double)calculateCaloriesFromMoves:(double)moves userWeight:(double)userWeight
{
    return moves * userWeight * 0.006;
}

+(double)calculateMovesFromDistance:(double)distance duration:(double)duration
{
    double mets = [self calculateMetsFromDistance:distance duration:duration];
    return (mets * (duration / 3600)) / 0.006;
}

+(double)calculateMovesFromMets:(double)mets duration:(double)duration
{
    double durationInMin = duration / 60.0;
    return ((mets) * 10 * durationInMin / 144) * 40;
}


@end
